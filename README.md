# README #


Quickstart for using Word macros to fix speech to text transcription errors

Here is some guidance to get you going - an explanation of the macro is beyond the scope of this article and I am by no means an expert in Word macros:

· Open Word>View>Macros > View Macros

· Enter a new name (I used DictationFix) in the first field of the popup window, then click create. A macro editor will open.

· Download the dictation fix text file from this page.

· Open the text file, select all (ctrl+a), copy (ctrl+c) the macro script.

· In the macro editor paste (ctrl+v) the script

· Click save

· Click the run key to execute the macro on your text.

To run the macro later:

· Open Word>View>Macros > View Macros

· Select DictationFix and click run



